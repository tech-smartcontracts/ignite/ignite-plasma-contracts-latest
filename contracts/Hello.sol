pragma solidity ^0.5.0;

import "./libraries/SafeMath.sol";

contract Hello {
	using SafeMath for uint256;

	uint256 userCount;

	mapping (address => string) public userPassword;

	constructor() public {
		userCount = 0;
	}

	event PasswordUpdated(address sender, string hash);
	
	function setNewPassword(address _sender, string memory _hash) public returns(bool) {
		require(msg.sender == _sender, 'Its your not account!');
		userPassword[msg.sender] = _hash;
		userCount = userCount.add(1);
		emit PasswordUpdated(_sender, _hash);
		return true;
	}
}
