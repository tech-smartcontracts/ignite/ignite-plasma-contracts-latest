pragma solidity ^0.5.0;

import "./libraries/SafeMath.sol";

contract CidBlock {
	using SafeMath for uint256;

	address public validator;
	uint256 public lastCommittedBlock;

	mapping(address => bool) public isWhiteList;
	mapping(uint256 => PlasmaBlock) public plasmaChain;

	struct PlasmaBlock {
		string btfsCid;
		string previousBtfsCid;
		address operator;
		uint256 createdAt;
	}

	event ToWhiteListAddressAdded(address operator, uint256 addedAt);
	event InWhiteListAddressRemoved(address operator, uint256 removedAt);
	event BlockSubmitted(string btfsCid, string previousBtfsCid, address operator);

	constructor() public {
		lastCommittedBlock = 0;
		validator = msg.sender; 
		isWhiteList[msg.sender] = true;
	}

	modifier isOperator() { 
		require(isWhiteList[msg.sender] == true, 'Your not have access, because you not operator!');
		_; 
	}

	modifier isValidator() { 
		require(msg.sender == validator, 'Your not have access, because you not validator!'); 
		_; 
	}
	
	function submitBlock(string memory _btfsCid) public isOperator {
		string memory previousBtfsCid = '0x0';
		if(lastCommittedBlock > 0) {
			PlasmaBlock memory previousBlock = plasmaChain[lastCommittedBlock];
			previousBtfsCid = previousBlock.btfsCid;
		}

		lastCommittedBlock = lastCommittedBlock.add(1);
		plasmaChain[lastCommittedBlock] = PlasmaBlock({
			btfsCid: _btfsCid, 
			previousBtfsCid: previousBtfsCid, 
			operator: msg.sender,
			createdAt: block.timestamp
		});

		emit BlockSubmitted(_btfsCid, previousBtfsCid, msg.sender);
	}

	function addToWhiteList(address operator) public isValidator {
		require(operator != address(0));
		isWhiteList[operator] = true;
		emit ToWhiteListAddressAdded(operator, now);
	}

	function removeInWhiteList(address operator) public isValidator {
		require(operator != address(0));
		isWhiteList[operator] = false;
		emit InWhiteListAddressRemoved(operator, now);
	}
}
