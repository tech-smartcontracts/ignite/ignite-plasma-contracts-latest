pragma solidity ^0.5.0;

import "./libraries/SafeMath.sol";

contract RootChain {
	using SafeMath for uint256;

	address owner;

	uint256 public lastSyncBlock;
	uint256 public lastVerificationBlock;

	mapping(address => bool) public isWhiteList;
	mapping(uint256 => VerificationBlock) public verificationBlocks;

	struct VerificationBlock {
		string verificationHash;
		address operator;
		uint256 createdAt;
	}

	event ToWhiteListAddressAdded(address operator, uint256 addedAt);
	event InWhiteListAddressRemoved(address operator, uint256 removedAt);
	event VerificationBlockSubmitted(string verificationHash, address operator);

	constructor() public {
		lastSyncBlock = 0;
		lastVerificationBlock = 0;
		owner = msg.sender; 
		isWhiteList[msg.sender] = true;
	}

	modifier isOperator() { 
		require(isWhiteList[msg.sender] == true, 'Your not have access, because you not operator!');
		_; 
	}

	modifier isOwner() { 
		require(msg.sender == owner, 'Your not have access, because you not owner!'); 
		_; 
	}

	function submitVerificationBlock(string memory _verificationHash, uint256 _lastSyncBlock) public isOperator {
		lastSyncBlock = _lastSyncBlock;
		lastVerificationBlock = lastVerificationBlock.add(1);
		verificationBlocks[lastVerificationBlock] = VerificationBlock({
			verificationHash: _verificationHash,
			operator: msg.sender,
			createdAt: block.timestamp
		});
		emit VerificationBlockSubmitted(_verificationHash, msg.sender);
	}
	
	function addToWhiteList(address operator) public isOwner {
		require(operator != address(0));
		isWhiteList[operator] = true;
		emit ToWhiteListAddressAdded(operator, now);
	}

	function removeInWhiteList(address operator) public isOwner {
		require(operator != address(0));
		isWhiteList[operator] = false;
		emit InWhiteListAddressRemoved(operator, now);
	}
}
